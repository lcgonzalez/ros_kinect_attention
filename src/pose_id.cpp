#include "ros/ros.h"
#include "sensor_msgs/image_encodings.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32MultiArray.h"

#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <sstream>
#include <iostream>
#include <vector>

//C++ object serialization.
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <termios.h>

int kbhit()
{
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}

using namespace cv_bridge;
using namespace std;
using namespace cv;

class BufferToggle
{
    private:
        struct termios t;

    public:

        /*
         * Disables buffered input
         */

        void off(void)
        {
            tcgetattr(STDIN_FILENO, &t); //get the current terminal I/O structure
            t.c_lflag &= ~ICANON; //Manipulate the flag bits to do what you want it to do
            tcsetattr(STDIN_FILENO, TCSANOW, &t); //Apply the new settings
        }


        /*
         * Enables buffered input
         */

        void on(void)
        {
            tcgetattr(STDIN_FILENO, &t); //get the current terminal I/O structure
            t.c_lflag |= ICANON; //Manipulate the flag bits to do what you want it to do
            tcsetattr(STDIN_FILENO, TCSANOW, &t); //Apply the new settings
        }
};

void replace(Mat &mat, float min, float max)
{
  unsigned char *data = (unsigned char*)(mat.data);
  unsigned int rows, cols;
  float value;
  rows = mat.rows;
  cols = mat.cols;
  for(int row=0; row < rows; row++) {
    float *ptr = (float*)(data + row*mat.step);
    for(int col=0; col < cols; col++, ptr++) {
     value = *ptr;
     //cout << value << endl;
     if(value > max) {
        *ptr = 0.0;
     }
    }
  }
}


class ImageConverter
{
private:
  ros::NodeHandle nh_;
  ros::Subscriber image_sub_;
  ros::Publisher image_pub_, attention_pub_, error_pub_;
  unsigned int framesPerShot_; //How often the frames will be fed to the classifier.
  unsigned int framesCount_;
  float  errorNoDep_;
  cv::Mat Im_;
  vector<cv::Mat> X_, x_pinv_;

  cv::FileStorage fs;
  BufferToggle bt;

  std_msgs::Float32MultiArray errorArray;

public:
  ImageConverter() : 
    fs("test.xml", cv::FileStorage::READ)
  {
    image_pub_ = nh_.advertise<sensor_msgs::Image>("output", 1);
    attention_pub_ = nh_.advertise<std_msgs::Int32>("attention", 1);
    error_pub_ = nh_.advertise<std_msgs::Float32MultiArray>("error", 1);
    image_sub_ = nh_.subscribe("input", 1,
        &ImageConverter::readImageCallback, this);
    framesPerShot_ = 5;
    framesCount_ = 0;
    errorNoDep_ = 5; 

    fs["X"] >> X_;
    fs["x_pinv"] >> x_pinv_;
    cout << "X size: " << X_.size() << endl;
    cout << "x_pinv size: " << x_pinv_.size() << endl;
    //initscr();
    //cbreak();
    //nodelay(stdscr, TRUE);
    bt.off();
  }

  ~ImageConverter()
  {
    bt.on();
  }

void readImageCallback(const sensor_msgs::ImageConstPtr& source)
{
  ROS_INFO("ROS image received.");
  CvImagePtr imagePtr;
  imagePtr = toCvCopy(source);

  float max,min;
  min = 0.5;
  max = 2.0;
  cv::Mat image1, dst, tmp, m, yAp;
  vector<float> error;
  //imagePtr->image.convertTo(image1, CV_8U, 255.0/(max-min), min);
  //imagePtr->image.convertTo(image1, CV_64F);
  cv::patchNaNs(imagePtr->image);
  imagePtr->image.convertTo(imagePtr->image, CV_32F);
  replace(imagePtr->image, min, max);
  image1 = imagePtr->image;
  tmp = image1;
  dst = tmp;
  //pyrDown(tmp, dst, cv::Size( tmp.cols/4, tmp.rows/4 ) );
  resize(image1, dst, cv::Size(), 0.05, 0.05, CV_INTER_AREA); 
  //ROS_INFO("Downsampled image size: %i, %i", dst.cols, dst.rows);
  tmp = dst.reshape(0, dst.cols*dst.rows);
  //ROS_INFO("Downsampled image size: %i, %i", tmp.cols, tmp.rows);
  vector<cv::Mat>::iterator x_pinv_it=x_pinv_.begin();
  int i = 0;
  for(vector<cv::Mat>::iterator Xit=X_.begin(); Xit != X_.end(); Xit++, 
      x_pinv_it++) {
  m = *x_pinv_it*tmp;
  yAp = *Xit*m; 
  error.push_back( cv::norm(yAp-tmp) );
  cout << "error class " << i << ": " << error.at(i++) << endl;
  }
  errorArray.data = error;
  error_pub_.publish(errorArray);
  float minError = error.at(0);
  int minErrorIndex = 1;
  i = 1;
  for(vector<float>::iterator errorIt = error.begin();
      errorIt < error.end(); errorIt++) {
    if(*errorIt < minError) {
      minError = *errorIt;
      minErrorIndex = i;
    }
    i++;
  }
  cout << "Class chosen: " << minErrorIndex << endl;
  std_msgs::Int32 msg;
  msg.data = minErrorIndex;
  attention_pub_.publish(msg);
  
  //std::cout << "error: " << error << std::endl;
  //ROS_INFO("Database size: %i, %i", Im_.cols, Im_.rows);
  //cv::SVD svd(Im_, cv::SVD::NO_UV);
  //ROS_INFO("Svdl: %f, %f", svd.w.at<float>(0,1), svd.w.at<float>(0,Im_.cols-1));
  //std::cout << svd.w << std::endl;
  
  //if(error > errorNoDep_) { //check the linear depedency of the last vector
   // ROS_INFO("New immage linearly independent");

    //Im_ = Im_.colRange(0, Im_.cols-1); 
  //} 
  if(kbhit()) { // read character only if one is ready to read
  if(getchar() == 'f') {
    ROS_INFO("Closing node");
    ros::shutdown();
  }
  }
  //std::cout << "max" << max << std::endl;
  //std::cout << "min" << min << std::endl;
  //std::cout << image1 << std::endl;

  //cv::circle(imagePtr->image, cv::Point(50, 50), 100, CV_RGB(255,0,0));

  dst.convertTo(image1, CV_8U, 255.0/(4.0 /*max-min*/), min);
  CvImage out_msg(imagePtr->header, "mono8", image1);
  image_pub_.publish(out_msg.toImageMsg());
  ROS_INFO("ROS image sent.");
}
};

/**
 * This tutorial demostrates simple image conversion from ROS message to OpenCV
 */
int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line. For programmatic
   * remappings you can use a different version of init() which takes remappings
   * directly, but for most command-line programs, passing argc and argv is the easiest
   * way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "pose_id");
  
  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ImageConverter ic;
  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}
